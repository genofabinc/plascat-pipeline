#  De novo assembly of plasmid sequences

Produces a de novo assembly for circular plasmids from paired-end Illumina sequences 
and/or Oxford Nanopore long read sequences.

Users must supply either:  

 * FWD_FASTQ and REV_FASTQ files to assemble short paired-end Illumina reads,  
 * a LONG_FASTQ file to assemble *basecalled* long Oxford Nanopore reads,  
 * or FWD_FASTQ, REV_FASTQ, and LONG_FASTQ files to generate a hybrid assembly.  

Users are encouraged to provide unique values for ASSEMBLY_NAME and OUTPUT_DIR  
to set the name for the assembly that will be used for the sequence header(s)  
in the output assembly file, and to organize output files from separate runs.  

We recommend users with long read data also provide an expected size for the final  
assembly to accurately subsample your reads to the desired coverage. By default,  
reads will be subsampled to 1000X coverage assuming a 6000 bp plasmid.   

This pipeline is now available as a web application!**  
Visit [PlasCAT](https://sequencing.genofab.com) to run multiple samples in parallel or to avoid the command line.**


# Prerequisite software

The denovo verification script requires the following to be installed on your system:  

 * **Linux or MacOS**
 * **Python3** between v3.6.9 and v3.11.6
 * **Trimmomatic** v0.39 (http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic)
 * **Filtlong** v0.2.1 (https://github.com/rrwick/Filtlong)
 * **rasusa** v0.7.1 (https://github.com/mbhall88/rasusa)
 * **SPAdes** v3.13.1 (http://cab.spbu.ru)
 * **Unicycler** v0.4.8 (https://github.com/rrwick/Unicycler.git)


## Trimmomatic
The verification script uses Trimmomatic for strict filtering of the short input reads.  

```
wget -O trimmomatic.zip http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.39.zip
unzip trimmomatic.zip
```

Once unpacked, you will want to ensure the trimmomatic jar file is in your Java path.  
Either that or you will need to specify the jar file location whenever you run the pipeline.  

## Filtlong
The verification script uses Filtlong to lightly filter the long input reads.  

```
git clone https://github.com/rrwick/Filtlong.git
cd Filtlong
make -j
bin/filtlong -h
```

To complete the installation you should copy `bin/filtlong` to a common user area such as `/usr/bin/filtlong`.  

## rasusa
Long reads are **ra**ndomly **su**b**sa**mpled to a desired coverage level to improve assembly quality and runtime.  

```
wget -nv -O - rasusa.mbh.sh | sh
```

Note that the seed value is set so although subsample generation is random, it also reproducible.  

## SPAdes 
The *Unicycler* pipeline uses SPAdes as its back end assembler.  

```
wget -O spades.tgz http://cab.spbu.ru/files/release3.15.5/SPAdes-3.15.5-Linux.tar.gz
tar zxf spades.tgz
```

In the SPAdes folder you should find the file `spades.py`.  To complete the installation  
you should copy that file to a common user area such as `/usr/bin/spades.py`.  


## Unicycler
This is the main workhorse for the verification script.  

```
git clone --depth=1 https://github.com/rrwick/Unicycler.git unicycler
cd unicycler 
python3 setup.py install 
```

# Quick start
To run the de novo verification script, use the following command:  

```
denovo_plasmid_assembly.py -1 good-forward_R1_001.fastq.gz -2 good-reverse_R2_001.fastq.gz -l barcode01_cat.fastq.gz -a test_assembly -vd output_dir -s 2000
```

Output from this command should look something like the following:  

```
21:13:34 java -jar trimmomatic-0.39.jar PE -phred33 good-forward_R1.fastq.gz good-reverse_R2.fastq.gz output_dir/good-forward_R1_paired.fastq output_dir/good-forward_R1_unpaired.fastq output_dir/good-reverse_R2_paired.fastq output_dir/good-reverse_R2_unpaired.fastq SLIDINGWINDOW:9:35.000 MINLEN:50
21:13:34 filtlong --max_length 20000 --keep_percent 95 barcode01_cat.fastq.gz | gzip > output_dir/barcode01_cat_filt.fastq.gz
21:13:35 rasusa -i output_dir/barcode01_cat_filt.fastq.gz -c 1000 -g 2000b -o output_dir/barcode01_cat_filt_sub.fastq.gz -s 222
21:13:35 unicycler -1 output_dir/good-forward_R1_paired.fastq -2 output_dir/good-reverse_R2_paired.fastq -l output_dir/barcode01_cat_filt_sub.fastq.gz -o output_dir --keep 0 --no_rotate

21:13:49 Reads from ['output_dir/good-forward_R1_paired.fastq', 'output_dir/good-reverse_R2_paired.fastq', 'output_dir/barcode01_cat_filt_sub.fastq.gz'] successfully assembled.
Assembly contains 1 contig of length 2375.
Total time 15.235 seconds.
The final assembly has been written to output_dir/final_assembly.fasta
```


# Files in this directory
This directory contains the verification Python script along with several pairs of FASTQ files for testing it under different scenarios.

## Main script

`denovo_plasmid_assembly.py`: sequence verification script

### Full usage

```                         
denovo_plasmid_assembly.py [-h] [-1 FWD_FASTQ] [-2 REV_FASTQ] [-l LONG_FASTQ] [-a ASSEMBLY_NAME] [-d OUTPUT_DIR] [-m MIN_LENGTH] [-w FILTER_WINDOW]
                          [--min-quality MIN_QUALITY] [--jar JAR_PATH] [--fastp] [--max_length MAX_LENGTH] [-p KEEP_PERCENT] [--rasusa] [-s EXPECTED_SIZE]
                          [-c COVERAGE] [--contam] [-v]

Options:
  -h, --help            show this help message and exit
  -1 FWD_FASTQ          Forward Illumina read in fastq format
  -2 REV_FASTQ          Reverse Illumina read in fastq format
  -l LONG_FASTQ         Long-read Oxford Nanopore read in fastq format (must be already basecalled)
  -a ASSEMBLY_NAME      Name for plasmid assembly [default: assembly]
  -d OUTPUT_DIR         Output directory [default: denovo_assembly]
  -m MIN_LENGTH         Minimum filtered short-read length [default: Trimmomatic=50, fastp=150]
  -w FILTER_WINDOW      Filtering sliding window size [default: 9]
  --min-quality MIN_QUALITY
                        Minimum filtering quality [default: Trimmomatic=35, fastp=38]
  --jar JAR_PATH        (Trimmomatic) Path to JAR file [default: trimmomatic-0.39.jar]
  --fastp               Use fastp trimming [default: False]
  --max_length MAX_LENGTH
                        (Filtlong) Maximum read length (bp) [default: 20000]
  -p KEEP_PERCENT       (Filtlong) Percentage of good reads to keep [default: 80]
  --rasusa              Randomly subsample your filtered reads [default: True]
  -s EXPECTED_SIZE      (rasusa) Expected size of final assembly (bp) [default: 6000]
  -c COVERAGE           (rasusa) Coverage to subsample long reads to [default: 1000]
  --contam              (Short reads) Provides an estimate of contamination likelihood [default: False]
  -v                    Verbose mode [default: False]
```

### Paired-end reads
The script was designed and optimized using paired-end Illumina reads in FASTQ format.  
An important aspect of FASTQ files is the quality scores assigned to every base call,  
which the script uses for the Trimmomatic filtering step.  

### Long reads
The script was designed and optimized using long read Nanopore data in FASTQ format  
that was basecalled using the Super Accurate basecalling method from the Dorado basecaller.  
For the highest quality reads, we recommend you use at least the High Accuracy basecalling  
model, rather than the Fast model (see here for more information:  
https://community.nanoporetech.com/technical_documents/data-analysis/v/datd_5000_v1_revr_22aug2016/basecalling-algorithms).

### Assembly name
The script assigns a name for the contigs in the final assembly file. The contig names will use  
the name provided, appended with a sequential number. When the assembly is successful in creating  
a single contig, the name will simply be _assembly-name_-1. However, if the assembly produces more  
than one contig, subsequent contigs will be named _assembly-name_-2, _assembly-name_-3, etc.    

## Script output
By default the script will create all output in the current working directory, but we recommend  
using the `-d` option to create an output directory.  
In that directory, the script will generate the following files:  

### Filtered read files

#### Short reads

The Trimmomatic filtering step will actually produce four files: two `_paired.fastq` files with read pairings  
retained, and two `_unpaired` files where some pairs may be missing the forward or reverse read.  
We use the `_paired` files with Unicycler.  

 * `<forward-name>_paired.fastq`
 * `<forward-name>_unpaired.fastq`
 * `<reverse-name>_paired.fastq`
 * `<reverse-name>_unpaired.fastq`

#### Long reads

The Filtlong filtering step produces a new compressed filtered read file:  

 * `<long-read-name>_filt.fastq.gz`

The rasusa subsampling step produces a new compressed subset read file from the filtered file:  

 * `<long-read-name>_filt_sub.fastq.gz`

### Assembly output files
When our script runs the assembly program, it generates six additional files:  

 * `filtlong.log` -- contains the Filtlong output: number of long reads before and after filtering.  
 * `rasusa.log` -- contains the rasusa output: number of reads kept and the resulting coverage after subsetting the long reads.  
 * `unicycler.log` -- contains all the Unicycler output from the assembly run.  
 * `assembly.gfa` -- contains the assembled graph in [Graphical Fragment Assembly (GFA) format](https://github.com/GFA-spec/GFA-spec)  
 * `assembly.fasta` -- contains the contigs in FASTA format.  
 * `final_assembly.fasta` -- simply `assembly.fasta` with sequence headers updated to show the assembly names.  


## Sample input sequences
We provide three sets of sample input files for testing the verification script:  
the first is an example of hybrid assembly where the short and long reads can be  
successfully assembled separately or together (`hybrid-assembly`);  

 * `good-short-reads/good-forward_R1.fastq.gz`, `good-short-reads/good-reverse_R1.fastq.gz`, `barcode01_cat.fastq.gz`. 

the second is two examples of short read only assemblies (`short-read_only`);  

 * `good_sample_forward.fastq.gz`, `good_sample_reverse.fastq.gz` : successful assembly  
 * `multi_contig_forward.fastq.gz`, `multi_contig_reverse.fastq.gz` : two different plasmids cannot be reconciled, so the assembler produces multiple contigs.  

and the third is two examples of contaminated reads for use with the `--contam` flag (`contamination-likelihood`);

 * `snp_contamination_35pct_forward.fastq.gz`, `snp_contamination_35pct_reverse.fastq.gz` : the primary plasmid has been mixed with a second plasmid that differs from the first by a single SNP;  
 * `indel_contamination_20pct_forward.fastq.gz`, `indel_contamination_20pct_reverse.fastq.gz` : the primary plasmid has been mixed with a second plasmid that differs from the first by an insertion.  


# Interpreting the script output

## What if my assembly is fragmented?
The script will report a fragmented assembly (an assembly having more than one contig) as contaminated.  
However, we have also found that Unicycler may generate multiple contigs if there are too many reads even  
after applying strict filtering criteria.  
Hence before rejecting a sample, we recommend re-running the pipeline by adjusting the filtering criteria.

**NOTE: filtering can be highly sensitive to these values, so we recommend making small changes.**

### With short reads:
There are three different parameters that contribute to the number and quality of reads in the filtered set.  
Generally speaking, increasing any of these values tends to decrease the number of reads in the filtered set,  
while decreasing them tends to increase the filtered set size.  

 * `-m MIN_LENGTH`: This controls the minimum filtered read length.  
 * `-w FILTER_WINDOW`:  This controls the size of the sliding window used in filtering.  Longer windows impose  
 stricter criteria on the sequences (longer stretches must maintain quality over the given threshold).  
 * `--min-quality=MIN_QUALITY`: This is the minimum quality that must be maintained within a sliding window.  

### With long reads
There are two different parameters that contribute to the number and quality of reads in the filtered set.  
In general, decreasing these values will increase the number of reads in the filtered set, while increasing  
them will increase the filtered set size.  

 * `--max_length MAX_LENGTH`: This controls the maximum filtered read length.  
 * `-p KEEP_PERCENT`: This controls the percentage of good reads to keep, based on both quality and length.  

### With subsampled reads
There are two different parameters that contribute to the number of reads in the subsampled set.  
In general, you should not change the expected size of your final assembly (if known). However, it can be  
as precise as you would like. In general, we recommend rounding the expected size to the nearest 1000 bp.  

 * `-s EXPECTED_SIZE`: This sets the expected size of final assembly.
 * `-c COVERAGE`: This controls the number of reads to keep based on the expected size. For example,  
 subsampling a 5000 bp plasmid to 1000X coverage would return roughly 5 million bases.

## How does the contamination likelihood estimate work? 
The script can optionally return two values related to potential contamination in a sample: the proportion of high-quality  
reads that fail to map to the assembly, and a likelihood that reflects where that proportion stands relative to good and bad assemblies.  
The likelihood scores have been established based on controlled scenarios where contamination may consist of a single SNP.  
This means that the estimated likelihood of contamination should be sensitive to any form of possible contamination.  
This estimated likelihood reaches 50% when more than 15% of reads fail to map to the assembly, and 90% when 20% or more reads fail to map.  
This feature is still under development. You may wish to confirm these results by comparing your assembly to a reference sequence.  

# Limitations of the pipeline

This pipeline has been developed and tested using over two dozen plasmids ranging in length from 2,521 to 14,194 bases.

In general, a failed or incomplete assembly is indicative of either:  
 * too little or poor quality data, or  
 * too much data.  

Low quality data impacts the amount of data available for an assembly. This becomes evident  
whenever the `*_paired.fastq` files output from Trimmomatic are small or empty.  

To compensate for this, as a rough guide, we recommend decrementing the `MIN_QUALITY` value whenever the number  
of pairs in the `*_paired.fastq` files drops much below 400 reads per thousand plasmid bases  
(e.g., below 2,500 pairs for a long plasmid roughly 10,000 bases long).  

On the other hand, too much data can make it difficult to get a good assembly. For this reason,  
by default, we subsample reads to roughly 1000X coverage. In cases where the assembly fails,  
it may be useful to either increase the `MIN_QUALITY` or decrease the `KEEP_PERCENT`  
parameters depending on your data type. You may also subsample to a lower coverage (~100X).  
  
Although the tool was designed to produce de novo assemblies, in cases like this, it is beneficial  
to have a reference sequence for comparison. It is then possible to use a tool such as BLAST to  
compare a final assembly with the reference to assess the quality of the assembly.  

We are eager to collect additional and varied data to improve this pipeline, so we encourage users  
who have difficulty generating assemblies to consider collaborating with us on a future, more robust version.  
Please contact the authors if you wish to collabrate on the next version.  

# Reference Information

Paper: _Rapid, robust plasmid verification by de novo assembly of short sequencing reads_ 

[Nucleic Acids Research (2020) 48:18 e106](https://doi.org/10.1093/nar/gkaa727)

Authors: Jenna E. Gallegos, Mark F. Rogers, Charlotte Cialek, Jean Peccoud

# Contact Us

https://support.genofab.com/kb-tickets/new
