#! /usr/bin/env python3
#
# denovo_plasmid_assembly.py
# de novo assemblies for circular plasmids from paired-end Illumina sequences.
#
# Copyright 2019 Mark F. Rogers (rogers@genofab.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from optparse import OptionParser
import argparse
import gzip
import os
import shutil
import subprocess
import sys
import time

DESCRIPTION = """
Produces a de novo assembly for circular plasmids from paired-end Illumina sequences
and/or Oxford Nanopore long-read sequences.

Users must supply either: 
-- FWD_FASTQ and REV_FASTQ files to assemble short paired-end Illumina reads,
-- a LONG_FASTQ file to assemble *basecalled* long Oxford Nanopore reads,
-- or FWD_FASTQ, REV_FASTQ, and LONG_FASTQ files to generate a hybrid assembly.

Users are encouraged to provide unique values for ASSEMBLY_NAME and OUTPUT_DIR
to set the name for the assembly that will be used for the sequence header(s)
in the output assembly file, and to organize output files from separate runs.

We recommend users with long-read data also provide an expected size for the final
assembly (in base pairs) to accurately subsample your reads to the desired coverage.
By default, reads will be subsampled to 1000X coverage assuming a 6000 bp plasmid. 


Prerequisites:

    * Trimmomatic (http://www.usadellab.org/cms/index.php?page=trimmomatic, also requires Java)
    - or -
    * fastp (https://github.com/OpenGene/fastp)

    * rasusa (https://github.com/mbhall88/rasusa)

    * Unicycler (https://github.com/rrwick/Unicycler) """

# Quality filtering defaults
DEFAULT_WINDOW = 9

# DNA complements plus nucleotide ambiguity codes (IUPAC):
COMPLEMENT = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'Y': 'R', 'R': 'Y', 'W': 'W', 'S': 'S',
              'K': 'M', 'M': 'K', 'D': 'H', 'V': 'B', 'H': 'D', 'B': 'V', 'X': 'X', 'N': 'N',
              'a': 'T', 'c': 'G', 'g': 'C', 't': 'A', 'y': 'R', 'r': 'Y', 'w': 'W', 's': 'S',
              'k': 'M', 'm': 'K', 'd': 'H', 'v': 'B', 'h': 'D', 'b': 'V', 'x': 'X', 'n': 'N'}

FASTX_INDICATORS = '>@'
FASTA_INDICATOR = '>'
FASTQ_EXTS = ['.fastq', '.fq']
GZIP_EXTS = ['.gz', '.gzip']

# Format-specific constants
FASTA_HEADER_CHAR = '>'

# External program values:
TRIMMOMATIC_PATH='trimmomatic-0.39.jar'
TRIM_FORMAT = "java -jar %s PE -phred33"
TRIMMOMATIC_DEFAULT_QUALITY = 35
TRIMMOMATIC_MIN_LENGTH = 50

# Example: fastp -i $FWD -o fwd_filtered.fastq -I $REV -O rev_filtered.fastq -5 -3 -W 9 -M 38 -l $MIN_LEN -e 38 -w 8
FASTP_FWD = 'fwd_filtered.fastq'
FASTP_REV = 'rev_filtered.fastq'
FASTP_DEFAULT_QUALITY = 38

FILTLONG_LOG = 'filtlong.log'
FILTLONG_MAX_LENGTH = 20000
FILTLONG_KEEP_PERC = 80

DEFAULT_COVERAGE = 1000
DEFAULT_PLASMID_SIZE = 5000
RASUSA_LOG = 'rasusa.log'

DEFAULT_START_SEED = 222
RETRY_START_SEED_1 = 333
RETRY_START_SEED_2 = 444

UNICYCLER_LOG = 'run_unicycler.log'
UNICYCLER_ASSEMBLY = 'assembly.fasta'
FINAL_ASSEMBLY = 'final_assembly.fasta'
UNICYCLER_OPTIONS = '--keep 0 --no_rotate'

# CDF estimated from bootstrapped combinations of 'primary' and 'contaminant' *short read* sequences with contaminant
# proportions ranging from 5% to 50% of total reads.  For assemblies with a single sequence, reads were compared
# to the assembly to find exact matches and mismatches.  All data sets yield some proportion of mismatches, but
# contaminated data sets yield higher proportions than clean data.  These (value,percentile) pairs reflect the
# estimated distribution for contaminated data sets.
CONTAMINATED_CDF = [(11.15,1), (11.43,2), (11.6,3), (11.8,4), (12.0,5), (12.2,6), (12.3,7), (12.4,8), (12.5,9), (12.6,10),
                    (12.7,11), (12.8,13), (12.9,14), (13.0,15), (13.1,16), (13.12,17), (13.2,18), (13.3,19), (13.4,20), (13.47,21),
                    (13.5,22), (13.6,23), (13.64,24), (13.7,25), (13.8,27), (13.9,28), (13.98,29), (14.1,30), (14.10,31), (14.2,32),
                    (14.3,34), (14.4,35), (14.5,37), (14.6,38), (14.7,39), (14.73,40), (14.8,41), (14.9,42), (15.0,44), (15.1,45),
                    (15.2,46), (15.3,47), (15.4,48), (15.5,49), (15.6,50), (15.7,51), (15.70,52), (15.8,53), (15.9,54), (16.0,55),
                    (16.09,56), (16.1,57), (16.2,58), (16.3,59), (16.4,61), (16.5,62), (16.6,63), (16.7,64), (16.8,65), (16.9,66),
                    (17.0,67), (17.1,68), (17.2,69), (17.21,70), (17.3,71), (17.4,72), (17.5,73), (17.56,74), (17.63,75), (17.7,76),
                    (17.8,77), (17.9,78), (18.0,79), (18.14,80), (18.3,81), (18.4,82), (18.55,83), (18.7,84), (18.8,85), (18.9,86),
                    (19.0,87), (19.3,88), (19.49,89), (19.80,90), (20.07,91), (20.34,92), (20.7,93), (20.90,94), (21.39,95), (21.91,96),
                    (22.4,97), (23.4,98), (24.68,99), (29.78,100)]


def assure_directory(d, verbose=False):
    """Guarantee that the given directory exists, or raise an exception if there is a problem with it."""
    if not os.path.isdir(d):
        if verbose:
            sys.stderr.write('Creating directory %s\n' % d)
        os.makedirs(d)
    return d


def compute_mismatch_percentage(reference_file, queries, max_readlen, verbose=False):
    """Finds reads in the query files that fail to match the reference sequence exactly."""
    # We use an expanded reference sequence to account for wrap-around
    # (circular assemblies), so this method only needs to detect reads not found
    # anywhere in the reference sequence -- simpler and faster than Smith-Waterman.
    expanded_assembly = load_expanded_assembly(reference_file, max_readlen)
    reverse_assembly = reverse_complement(expanded_assembly)
    mismatches = 0
    for q in queries:
        if q in expanded_assembly or q in reverse_assembly:
            continue
        mismatches += 1

    return mismatches * 100.0 / len(queries)


def denovo_assembly(fwd_fastq=None, rev_fastq=None, long_fastq=None, output_dir='output_dir', verbose=False):
    """Runs Unicycler to produce a de novo assembly from the given paired-end FASTQ files
    and places the results in the output directory."""
    cmd = 'unicycler'
    if fwd_fastq is None and rev_fastq is None and long_fastq is None:
        arg_parser.print_help()
        sys.exit(1)
    if fwd_fastq is not None:
        cmd += ' -1 %s' % fwd_fastq
    if rev_fastq is not None:
        cmd += ' -2 %s' % rev_fastq
    if long_fastq is not None:
        cmd += ' -l %s' % long_fastq
    cmd += ' -o %s %s' % (output_dir, UNICYCLER_OPTIONS)
    run_command(cmd, verbose=verbose)
    return os.path.join(output_dir, UNICYCLER_ASSEMBLY)

def estimate_likelihood(misalign_pct, cdf):
    """Returns the estimated likelihood (area under cdf) for the given misalignment percentage."""
    for (pct, percentile) in cdf:
        if misalign_pct < pct:
            return percentile
    return 100


def ezopen(fileName) :
    """Open files even if they're gzipped."""
    if not (os.path.exists(fileName) and os.path.isfile(fileName)):
        raise ValueError('file does not exist at %s' % fileName)

    handle = gzip.open(fileName, mode='rt')
    try :
        line = handle.readline()
        handle.close()
        return gzip.open(fileName, mode='rt')
    except :
        return open(fileName)


def fastp(fwd_fastq, rev_fastq, min_length, window_size, min_quality, dest_dir, verbose=False):
    """Runs fastp to filter FASTQ reads using a sliding window and a minimum final length."""

    fwd_base = find_file_prefix(fwd_fastq)
    rev_base = find_file_prefix(rev_fastq)

    fwd_filtered = os.path.join(dest_dir, FASTP_FWD)
    rev_filtered = os.path.join(dest_dir, FASTP_REV)

    # Example: fastp -i forward.fastq -o fwd_filtered.fastq -I reverse.fastq -O rev_filtered.fastq -5 -3 -W 9 -M 38 -l 150 -e 38 -w 8
    cmd = "fastp -i %s -o %s -I %s -O %s -5 -3 -W %d -M %d -l %d -e %d" % \
            (fwd_fastq, fwd_filtered, rev_fastq, rev_filtered, window_size, min_quality, min_length, min_quality)
    run_command(cmd, verbose=verbose)

    return fwd_filtered, rev_filtered


def filtlong(fastq_path, max_length, keep_percent, dest_dir, verbose=False):
    """Light filtering of long-reads"""
    fastq_name = find_file_prefix(fastq_path)
    filtered_path = os.path.join(dest_dir, '%s_filt.fastq.gz' % fastq_name)
    filtlong_log = os.path.join(dest_dir, FILTLONG_LOG)
    cmd = 'filtlong --max_length %d --keep_percent %d %s | gzip > %s' % (max_length, keep_percent, fastq_path, filtered_path)
    status = run_command(cmd, stdout_file=filtlong_log, verbose=verbose)

    return filtered_path, status


def find_file_prefix(fastq_file):
    """Finds the file prefix: for a file named foo.fastq, foo.fastq.gz, foo.fq or foo.fq.gz this returns just 'foo'.
    This is inelegant but should work for most FASTQ/gzipped FASTQ files."""
    result = os.path.basename(fastq_file)
    for gz_ext in GZIP_EXTS:
        if result.endswith(gz_ext):
            result = result[:-len(gz_ext)]

    for fq_ext in FASTQ_EXTS:
        if result.endswith(fq_ext):
            result = result[:-len(fq_ext)]
    return result


def load_expanded_assembly(assembly_file, max_readlen):
    """Loads a reference assembly and returns an expanded version based on the extension length."""
    extension_length = max_readlen - 1
    result = None
    for n, s, ignore in read_fastx(assembly_file):
        if result is None:
            if args.fwd_fastq is not None and args.long_fastq is None:
                if len(s) < extension_length:
                    print('** Warning! Some reads are longer than the assembly %s.' % assembly_file)
            result = s + s[:extension_length]
        else:
            raise ValueError('Found multiple sequences in assembly file')
    return result


def load_queries(*args):
    """Load all the query sequences from the given list.  Returns the sequences along with their maximum size."""
    max_length = 0
    queries = []
    for query_file in args:
        if query_file is not None:
            try:
                for n, s, q in read_fastx(query_file):
                    max_length = max(max_length, len(s))
                    queries.append(s)
            except IndexError:
                raise ValueError('Unable to load sequences from %s\n' % query_file)
    return queries, max_length


def post_process_assembly(input_fasta, output_fasta, prefix):
    """Augments headers in a FASTA file using the given prefix
    and returns the number of sequences found in the file."""
    records = [s.strip() for s in open(input_fasta)]
    fragment_count = 0
    linear_frags = []
    fragment_len = 0
    fragment_lengths = []
    total_bases = 0
    ostr = open(output_fasta, 'w')
    for s in records:
        if s.startswith(FASTA_HEADER_CHAR):
            if fragment_count > 0:
                fragment_lengths.append(fragment_len)
                fragment_len = 0
            ostr.write('%s%s-%s\n' % (FASTA_HEADER_CHAR, prefix, s[1:]))
            fragment_count += 1
            if not "circular=true" in s:
                linear_frags.append(s[1])
        else:
            ostr.write('%s\n' % s)
            fragment_len += len(s)
            total_bases += len(s)
    if fragment_count > 0:
        fragment_lengths.append(fragment_len)
    
    ostr.close()
    return fragment_count, fragment_lengths, total_bases, linear_frags


def short_read_count_warnings(queries, args):
    """Convenience method for reporting possibly too few or too many reads to run an assembly."""
    half_size = len(queries)/2
    if half_size < 1000:
        print('\n  ** Filtering with minimum quality %d and window size %d yields %d filtered read pairs.' % (args.min_quality, args.filter_window, half_size))
    elif half_size > 10000:
        print('\n  ** Filtering with minimum quality %d and window size %d yields %d filtered read pairs.' % (args.min_quality, args.filter_window, half_size))


def rasusa(fastq_path, expected_size, coverage, dest_dir, seed, verbose=False):
    """Randomly subsamples long-reads to desired coverage."""
    rasusa_log = os.path.join(dest_dir, RASUSA_LOG)
    # For a single long read
    if isinstance(fastq_path, str):
        fastq_name = find_file_prefix(fastq_path)
        subset_path = os.path.join(dest_dir, '%s_sub.fastq.gz' % fastq_name)
        cmd = 'rasusa -i %s -c %d -g %db -o %s -s %d' % (fastq_path, coverage, expected_size, subset_path, seed)
        
        status = run_command(cmd, stdout_file=rasusa_log, verbose=verbose)
        return subset_path, status

    # For paired-end reads
    elif isinstance(fastq_path, tuple):
        fastq_path1, fastq_path2 = fastq_path
        fastq_name = find_file_prefix(fastq_path1)
        subset_path1 = os.path.join(dest_dir, '%s_R1_sub.fastq.gz' % fastq_name)
        subset_path2 = os.path.join(dest_dir, '%s_R2_sub.fastq.gz' % fastq_name)
        cmd = 'rasusa -i %s %s -c %d -g %db -o %s %s -s %d' % (fastq_path1, fastq_path2, coverage, expected_size, subset_path1, subset_path2, seed)

        status = run_command(cmd, stdout_file=rasusa_log, verbose=verbose)
        return subset_path1, subset_path2, status


def read_fastx(fastx_path):
    """ read a FASTA/FASTQ sequence file """
    # Note: converts sequences to upper-case to avoid 'a' vs. 'A' issues
    def read_fasta(f):
        """ read a fasta file """
        seq_id = ''
        sequence = ''
        for l in f:
            if l.startswith('>'):
                if sequence:
                    yield seq_id, sequence, ''
                seq_id = l.strip()[1:].split()[0]
                sequence = ''
            else:
                sequence += l.strip()

        yield seq_id, sequence.upper(), None

    def read_fastq(f):
        """ read a fastq file """
        # Each record consists of four lines:
        #    sequence id / sequence / sequence id (duplicate) / quality encoding
        for l in f:
            seq_id = l.strip()[1:].split()[0]
            sequence = f.readline()
            s3 = f.readline()  # ignore
            phred_score = f.readline()
            yield seq_id.strip(), sequence.strip().upper(), phred_score.strip()

    # test if fasta or fastq
    fastx_stream = ezopen(fastx_path)
    first_line = fastx_stream.readline()
    assert(type(first_line) == str)
    fastx_stream.close()

    first_char = first_line[0]
    if first_char not in FASTX_INDICATORS:
        raise ValueError('%s is not a FASTA/FASTQ file' % fastx_path)

    fastx_stream = ezopen(fastx_path)

    # iterate over sequences in the file
    if first_char == FASTA_INDICATOR:
        for sId, sSeq, ignore in read_fasta(fastx_stream):
            yield sId, sSeq, ignore
    else:
        for sId, sSeq, sQual in read_fastq(fastx_stream):
            yield sId, sSeq, sQual


def reverse_complement(s):
    """Returns the reverse-complement of a DNA sequence."""
    return ''.join([COMPLEMENT[x] for x in s[::-1]])


def run_command(s, err_stream=subprocess.STDOUT, out_stream=subprocess.PIPE, stdout_file=None, cwd=None, verbose=False):
    """Simple wrapper method for running shell commands."""
    if verbose:
        sys.stderr.write(time_string('%s\n' % s))
    
    out_stream_arg = out_stream if stdout_file is None else open(stdout_file, 'w')

    status = subprocess.call(s, shell=True, stderr=err_stream, stdout=out_stream_arg, universal_newlines=True, cwd=cwd)

    if status != 0:
        sys.stderr.write(time_string("Error in the execution of the following command:\n%s\n" % s))
    
    if out_stream_arg != subprocess.PIPE:
        out_stream_arg.close()

    return status


def time_string(s, format_string='%X', lf=False):
    """Returns the input string with a user-readable timestamp prefix."""
    timestamp = time.strftime(format_string, time.localtime())
    result = '%s %s' % (timestamp, s)
    if lf:
        result += '\n'
    return result


def trimmomatic(fwd_fastq, rev_fastq, min_length, window_size, min_quality, dest_dir, jar_path, verbose=False):
    """Runs Trimmomatic to filter FASTQ reads using a sliding window and a minimum final length."""

    fwd_base = find_file_prefix(fwd_fastq)
    rev_base = find_file_prefix(rev_fastq)

    fwd_paired = os.path.join(dest_dir, '%s_paired.fastq' % fwd_base)
    fwd_unpaired = os.path.join(dest_dir, '%s_unpaired.fastq' % fwd_base)
    rev_paired = os.path.join(dest_dir, '%s_paired.fastq' % rev_base)
    rev_unpaired = os.path.join(dest_dir, '%s_unpaired.fastq' % rev_base)

    options_string = 'SLIDINGWINDOW:%d:%.3f MINLEN:%d' % (window_size, min_quality, min_length)

    trim_prefix = TRIM_FORMAT % jar_path

    cmd = '%s %s %s %s %s %s %s %s' % (trim_prefix, fwd_fastq, rev_fastq, fwd_paired, fwd_unpaired, rev_paired, rev_unpaired, options_string)
    run_command(cmd, verbose=verbose)

    return fwd_paired, rev_paired


def validate_file(path):
    """Standard method for validating file paths."""
    if not path:
        raise Exception("'%s' is not a valid file path; exiting." % path)

    if not os.path.exists(path):
        raise Exception("File '%s' not found; exiting." % path)


# Establish command-line options:
arg_parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=DESCRIPTION)
arg_parser.add_argument('-1', dest='fwd_fastq', default=None, help='Forward Illumina read in fastq format')
arg_parser.add_argument('-2', dest='rev_fastq', default=None, help='Reverse Illumina read in fastq format')
arg_parser.add_argument('-l', dest='long_fastq', default=None, help='Long-read Oxford Nanopore read in fastq format (must be already basecalled)')
arg_parser.add_argument('-a', dest='assembly_name', default='assembly', help='Name for plasmid assembly [default: %(default)s]')
arg_parser.add_argument('-d', dest='output_dir', default='denovo_assembly', help='Output directory [default: %(default)s]')
arg_parser.add_argument('-m', dest='min_length', default=None, help='Minimum filtered short-read length [default: Trimmomatic=50, fastp=150]', type=int)
arg_parser.add_argument('-w', dest='filter_window', default=DEFAULT_WINDOW, help='Filtering sliding window size [default: %(default)s]', type=int)
arg_parser.add_argument('--min-quality', dest='min_quality', default=None, help='Minimum filtering quality [default: Trimmomatic=35, fastp=38]', type=float)
arg_parser.add_argument('--jar', dest='jar_path', default=TRIMMOMATIC_PATH, help='(Trimmomatic) Path to JAR file [default: %(default)s]')
arg_parser.add_argument('--fastp', dest='fastp', default=False, help='Use fastp trimming [default: %(default)s]', action='store_true')
arg_parser.add_argument('--max_length', dest='max_length', default=FILTLONG_MAX_LENGTH, type=int, help='(Filtlong) Maximum read length (bp) [default: %(default)d]')
arg_parser.add_argument('-p', dest='keep_percent', default=FILTLONG_KEEP_PERC, type=int, help='(Filtlong) Percentage of good reads to keep [default: %(default)d]')
arg_parser.add_argument('--rasusa', dest='random_subsample', default=True, help='Randomly subsample your filtered reads [default: %(default)s]', action='store_false')
arg_parser.add_argument('-s', dest='expected_size', default=DEFAULT_PLASMID_SIZE, type=int, help='(rasusa) Expected size of final assembly (bp) [default: %(default)d]')
arg_parser.add_argument('-c', dest='coverage', default=DEFAULT_COVERAGE, type=int, help='(rasusa) Coverage to subsample reads to [default: 1000]')
arg_parser.add_argument('--contam', dest='contamination', default=False, help='(Short reads) Provides an estimate of contamination likelihood [default: %(default)s]', action='store_true')
arg_parser.add_argument('-v', dest='verbose', default=False, help='Verbose mode [default: %(default)s]', action='store_true')
args = arg_parser.parse_args()

filtlong_path = shutil.which('filtlong')
if not filtlong_path:
    arg_parser.print_help()
    sys.stderr.write('\n** Cannot find Filtlong in your path!  Is it installed?\n\n')
    sys.exit(1)
    
rasusa_path = shutil.which('rasusa')
if not rasusa_path:
    arg_parser.print_help()
    sys.stderr.write('\n** Cannot find rasusa in your path!  Is it installed?\n\n')
    sys.exit(1)

unicycler_path = shutil.which('unicycler')
if not unicycler_path:
    arg_parser.print_help()
    sys.stderr.write('\n** Cannot find unicycler in your path!  Is it installed?\n\n')
    sys.exit(1)

if args.fwd_fastq:
    try:
        validate_file(args.fwd_fastq)
    except:
        sys.stderr.write('\nForward read %s could not be validated.\n\n' % args.rev_fastq)
        sys.exit(1)
    if args.rev_fastq:
        try:
            validate_file(args.rev_fastq)
        except: 
            sys.stderr.write('\nReverse read %s could not be validated.\n\n' % args.rev_fastq)
            sys.exit(1)
    else:
        sys.stderr.write('\nForward AND reverse fastq files must be provided.\n\n')
        sys.exit(1)
    #Looking at short read length (usually 151 or 251 bp) to determine minimum read length
    queries_pre, max_readlen = load_queries(args.fwd_fastq, args.rev_fastq)
    if max_readlen > 250:
        FASTP_MIN_LENGTH = 150
    elif max_readlen < 250:
        FASTP_MIN_LENGTH = 75

assure_directory(args.output_dir)

start_time = time.time()

# Filter short reads
if args.fastp:
    fastp_path = shutil.which('fastp')
    if not fastp_path:
        arg_parser.print_help()
        sys.stderr.write('\n** Cannot find fastp in your path!  Is it installed?\n\n')
        sys.exit(1)

    print(time_string('Using fastp for trimming.'))
    if args.min_quality is None:
        args.min_quality = FASTP_DEFAULT_QUALITY

    if args.min_length is None:
        args.min_length = FASTP_MIN_LENGTH

    fwd_filtered, rev_filtered = fastp(args.fwd_fastq, args.rev_fastq, args.min_length,
                                          args.filter_window,
                                          args.min_quality,
                                          args.output_dir,
                                          verbose=args.verbose)
elif args.fwd_fastq:
    java_path = shutil.which('java')
    if not java_path:
        arg_parser.print_help()
        sys.stderr.write('\n** Cannot find java in your path!  Is it installed?\n\n')
        sys.exit(1)

    if not os.path.exists(args.jar_path):
        raise ValueError('Trimmomatic JAR file %s not found' % args.jar_path)

    if args.min_quality is None:
        args.min_quality = TRIMMOMATIC_DEFAULT_QUALITY

    if args.min_length is None:
        args.min_length = TRIMMOMATIC_MIN_LENGTH
    fwd_filtered, rev_filtered = trimmomatic(args.fwd_fastq, args.rev_fastq, args.min_length,
                                          args.filter_window,
                                          args.min_quality,
                                          args.output_dir,
                                          args.jar_path,
                                          verbose=args.verbose)
else:
    fwd_filtered = None
    rev_filtered = None

# Filter long reads
if args.long_fastq:
    try:
        validate_file(args.long_fastq)
    except:
        sys.stderr.write('\nLong read file %s could not be validated.\n\n' % args.long_fastq)
        sys.exit(1)
    # Filter long reads
    if args.max_length is None:
        args.max_length = FILTLONG_MAX_LENGTH

    if args.keep_percent is None:
        args.keep_percent = FILTLONG_KEEP_PERC

    filtered_long_fastq, filtlong_log = filtlong(args.long_fastq, args.max_length, args.keep_percent, args.output_dir, verbose=args.verbose)
else:
    filtered_long_fastq = None

# Subsample reads
if args.random_subsample is True:
    if args.coverage is None:
        args.coverage = DEFAULT_COVERAGE
    if args.expected_size is None:
        args.expected_size = DEFAULT_PLASMID_SIZE
    
    if fwd_filtered is not None:
        subset_fwd_fastq, subset_rev_fastq, rasusa_log = rasusa((fwd_filtered, rev_filtered), args.expected_size, args.coverage, args.output_dir, DEFAULT_START_SEED, verbose=args.verbose)
    else:
        subset_fwd_fastq = None
        subset_rev_fastq = None
    if filtered_long_fastq is not None:
        subset_long_fastq, rasusa_log = rasusa(filtered_long_fastq, args.expected_size, args.coverage, args.output_dir, DEFAULT_START_SEED, verbose=args.verbose)
    else:
        subset_long_fastq = None
else:
    subset_fwd_fastq = None
    subset_rev_fastq = None
    subset_long_fastq = None


read_list = []
if subset_fwd_fastq is not None:
    read_list.extend([subset_fwd_fastq, subset_rev_fastq])
elif fwd_filtered is not None:
    read_list.extend([fwd_filtered, rev_filtered])

if subset_long_fastq is not None:
    read_list.extend([subset_long_fastq])
elif filtered_long_fastq is not None:
    read_list.extend([filtered_long_fastq])

queries, max_readlen = load_queries(*read_list)

filter_endtime = time.time()
filter_time = filter_endtime - start_time

try:
    assembly_fasta = denovo_assembly(fwd_filtered, rev_filtered, subset_long_fastq, args.output_dir, verbose=args.verbose)
    assembly_endtime = time.time()
    assembly_time = assembly_endtime - filter_endtime
except OSError as ose:
    short_read_count_warnings(queries, args)

try:
    validate_file(assembly_fasta)
except:
    print(f"Assembly failed. See unicycler.log for more information.\n\n")
    #if long reads, generate new subsample and assembly
    if subset_long_fastq is not None:
        print("Attempting to generate another assembly...")
        new_subset_long_fastq, rasusa_log = rasusa(filtered_long_fastq, args.expected_size, args.coverage, args.output_dir, seed=RETRY_START_SEED_1, verbose=args.verbose)
        try:
            assembly_fasta = denovo_assembly(fwd_filtered, rev_filtered, new_subset_long_fastq, args.output_dir, verbose=args.verbose)
            assembly_endtime = time.time()
            assembly_time = assembly_endtime - filter_endtime
            validate_file(assembly_fasta)
        except:
            print("Final attempt to generate another assembly...")
            new_subset_long_fastq_2, rasusa_log = rasusa(filtered_long_fastq, args.expected_size, args.coverage, args.output_dir, seed=RETRY_START_SEED_2, verbose=args.verbose)
            try:
                assembly_fasta = denovo_assembly(fwd_filtered, rev_filtered, new_subset_long_fastq_2, args.output_dir, verbose=args.verbose)
                assembly_endtime = time.time()
                assembly_time = assembly_endtime - filter_endtime
                validate_file(assembly_fasta)
            except:
                print("Could not generate assembly. See unicycler.log for more information.\n\n")

final_fasta = os.path.join(args.output_dir, FINAL_ASSEMBLY)
fragments, contig_len, assembly_len, linear_frags = post_process_assembly(assembly_fasta, final_fasta, args.assembly_name)


print(time_string("Reads from %s successfully assembled." % read_list))
if fragments == 1:
    print('Assembly contains %d contig of length %d.' % (fragments, assembly_len))
else:
    print('** Assembly contains %d fragments (%s) totalling %d bases.' % (fragments, ', '.join(map(str, contig_len)), assembly_len))
    if fwd_filtered is not None: 
        short_read_count_warnings(queries, args)

if args.fwd_fastq:
    if linear_frags != []:
        linear_frags = ', '.join(linear_frags)
        print('** Warning!  Fragment(s) may be linear: #%s' % linear_frags)

if args.contamination is True:
    # Run mismatch analysis using filtered subset:
    percent = compute_mismatch_percentage(assembly_fasta, queries, max_readlen, verbose=args.verbose)
    contamination_likelihood = estimate_likelihood(percent, CONTAMINATED_CDF)
    print('%.1f%% realignment mismatches (%d%% estimated likelihood of contamination)' % (percent, contamination_likelihood))

end_time = time.time()
total_time = end_time - start_time

print('Total time %.3f seconds.' % total_time)
print('The final assembly has been written to %s' % final_fasta)
